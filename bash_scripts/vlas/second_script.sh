#!/bin/bash

# Coloured variables for script
# check whether parameters are more than four
if [ $4 ]; then
# start loop that displays parameters	
	for n in $@ 
		do 
		# check whether carrent  variable is equal third parameter 
		[ $n = $3  ] && continue #skip iteration 
		printf $n' ' #display current parameter
	done
	echo
else
	for n in $@ 
		do 
		printf $n' '
	done
	echo
fi
